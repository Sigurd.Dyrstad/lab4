package datastructure;

import cellular.CellState;

import java.util.ArrayList;
import java.util.List;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private CellState[][] cellGrid;

    public CellGrid(int rows, int columns, CellState initialState) {

        this.rows = rows;
        this.columns = columns;

        if (rows <= 0 || columns <= 0)
            throw new IllegalArgumentException();

        this.cellGrid = new CellState[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                cellGrid[i][j] = initialState;
            }
        }
    }

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || row >= numRows())
            throw new IndexOutOfBoundsException();

        if (column < 0 || column >= numColumns())
            throw new IndexOutOfBoundsException();

        cellGrid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        if (row < 0 || row >= numRows())
            throw new IndexOutOfBoundsException();

        if (column < 0 || column >= numColumns())
            throw new IndexOutOfBoundsException();

        return cellGrid[row][column];
    }

    @Override
    public IGrid copy() {
       CellGrid cellGridCopy = new CellGrid(rows, columns, null);

       for (int i = 0; i < numRows(); i++) {
           for (int j = 0; j < numColumns(); j++) {
               cellGridCopy.set(i, j, this.get(i,j));
           }
       }

       return cellGridCopy;
    }
}
